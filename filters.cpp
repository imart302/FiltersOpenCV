#include <iostream>
#include <cmath>
#include <vector>
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>

#define PI 3.14159265359

//Calculates the median of an array of integers
float median(int n, int* x);

//Makes the median filter for one channel image where k is the size of neighborhood
void medianFilter(cv::Mat &input, cv::Mat &out, unsigned int k);

//Generates a normalized gaussian kernel with neighborhood k and given sigma
cv::Mat gaussianKernelGenerator(int k, double sigma, bool norm = true);

//Convolves one channel image with the given kernel(must be odd)
void convolve2D(cv::Mat &input, cv::Mat &out, cv::Mat ker);

//Floating point version convolution
void convolvef2D(cv::Mat &input, cv::Mat &out, cv::Mat ker);

//Convolves an three channel image with the given kernel(odd size)
void convolveColor2D(cv::Mat &input, cv::Mat &out, cv::Mat ker);

//average filter
void averageFilter(cv::Mat &input, cv::Mat &out, int k);

//applies gaussian filter, with normalized gaussian kernel
void weightavgFiltering(cv::Mat &input, cv::Mat &out, int k, float sigma);

//gaussian filter over color image
void avgcolorFiltering(cv::Mat &input, cv::Mat &out, int k);

//calculates the sobel filter, if direction is 0 then caculates in x, otherwise is y direction
void sobelFiltering(cv::Mat &input, cv::Mat &out, int direction);

//calculates the prewitt filter, if direction is 0 then caculates in x, otherwise is y direction
void prewittFiltering(cv::Mat &input, cv::Mat &out, int direction);

//Calculetes sobel x, y direction, the magnitude, and the direction
void sobelAll(cv::Mat &input, cv::Mat &gx, cv::Mat &gy, cv::Mat &magn, cv::Mat &direct);

//Calculetes prewitt x, y direction, the magnitude, and the direction
void prewittAll(cv::Mat &input, cv::Mat &gx, cv::Mat &gy, cv::Mat &magn, cv::Mat &direct);

//generates a laplacian kernel with the aproximation of difference of gaussians
cv::Mat laplacianDoG(int k, float sigma1, float sigma2, bool norm = true);

//laplacian of gaussian filter, using aproximation of DoG.
void loGFiltering(cv::Mat &input, cv::Mat &output, double sigma1, double sigma2, int k);

//laplacian filter (second derivate)
void laplacianFiltering(cv::Mat &input, cv::Mat &output);

//Makes and edge detector, if g_option is equals 0 then apply sobel, otherwise prewitt
//threshold indicates the value for consider an edge, threshold must be between 0 and 1.
void edgeDetector(cv::Mat& input, cv::Mat& out, int g_option, double threshold);

//generates an image with vectors of the first derivative (magnitude and direction)
//step is the pixel jumps to draw an arrow.
void drawVectors(cv::Mat &edges, cv::Mat &draw, cv::Mat &dx, cv::Mat &dy, int step);

void testSobel(cv::Mat &gray_img);

void testPrewitt(cv::Mat &gray_img);

void testLaplacian(cv::Mat &gray_img);

void testEdgeDetector(cv::Mat &gray_img);

int main(int argc, char* argv[]){
    if(argc !=2){
        std::cout << "Must provide the input image as argument \n";
        return -1;
    }
    cv::Mat image;
    image = cv::imread(argv[1], CV_LOAD_IMAGE_COLOR);
    cv::Mat gray; 
    cv::cvtColor(image, gray, cv::COLOR_RGB2GRAY);

    testSobel(gray);
    testPrewitt(gray);
    testLaplacian(gray);
    testEdgeDetector(gray);
    
    return 0;
}

void testEdgeDetector(cv::Mat &gray_img){
    cv::Mat edges;
    cv::Mat conv;
    edgeDetector(gray_img, edges, 0, 0.25);


    edges.convertTo(conv, CV_8U, 255.0);
    cv::imwrite("edges_sobel_t25.jpg", conv);
    cv::namedWindow("Edges Sobel", CV_WINDOW_FREERATIO);
    cv::imshow("Edges Sobel", edges);
    cv::waitKey(0);
    cv::destroyWindow("Edges Sobel");

    edgeDetector(gray_img, edges, 1, 0.25);


    edges.convertTo(conv, CV_8U, 255.0);
    cv::imwrite("edges_prewitt_t25.jpg", conv);
    cv::namedWindow("Edges Prewitt", CV_WINDOW_FREERATIO);
    cv::imshow("Edges Prewitt", edges);
    cv::waitKey(0);
    cv::destroyWindow("Edges Prewitt");

    cv::Mat gx, gy, magn, direc;
    prewittAll(gray_img, gx, gy, magn, direc);
    cv::normalize(magn, magn, 0, 1, cv::NORM_MINMAX);
    cv::Mat drawed;
    drawVectors(edges, drawed, gx, gy, 5);
    drawed.convertTo(conv, CV_8UC3, 255.0);
    cv::imwrite("edges_vectors.jpg", conv);
    cv::namedWindow("Vectors", CV_WINDOW_FREERATIO);
    cv::imshow("Vectors", drawed);
    cv::waitKey(0);
    cv::destroyWindow("Vectors");    

}

void testSobel(cv::Mat &gray_img){
    cv::Mat gx, gy;
    cv::Mat concat;
    sobelFiltering(gray_img, gx, 0);
    sobelFiltering(gray_img, gy, 1);

    cv::normalize(gx, gx, 0, 1, cv::NORM_MINMAX, CV_64FC1);
    cv::normalize(gy, gy, 0, 1, cv::NORM_MINMAX, CV_64FC1);

    
    cv::Mat conv;
    gx.convertTo(conv, CV_8U, 255.0);
    cv::imwrite("sobel_x.jpg", conv);
    gy.convertTo(conv, CV_8U, 255.0);
    cv::imwrite("sobel_y.jpg", conv);


    cv::namedWindow("Sobel X", CV_WINDOW_FREERATIO);
    cv::namedWindow("Sobel Y", CV_WINDOW_FREERATIO);
    cv::imshow("Sobel X", gx);
    cv::imshow("Sobel Y", gy);
    cv::waitKey(0);
    cv::destroyWindow("Sobel X");
    cv::destroyWindow("Sobel Y");
}

void testPrewitt(cv::Mat &gray_img){
    cv::Mat gx, gy;
    cv::Mat concat;
    sobelFiltering(gray_img, gx, 0);
    sobelFiltering(gray_img, gy, 1);

    cv::normalize(gx, gx, 0, 1, cv::NORM_MINMAX, CV_64FC1);
    cv::normalize(gy, gy, 0, 1, cv::NORM_MINMAX, CV_64FC1);

    
    cv::Mat conv;
    gx.convertTo(conv, CV_8U, 255.0);
    cv::imwrite("prewitt_x.jpg", conv);
    gy.convertTo(conv, CV_8U, 255.0);
    cv::imwrite("prewitt_y.jpg", conv);


    cv::namedWindow("Prewitt X", CV_WINDOW_FREERATIO);
    cv::namedWindow("Prewitt Y", CV_WINDOW_FREERATIO);
    cv::imshow("Prewitt X", gx);
    cv::imshow("Prewitt Y", gy);
    cv::waitKey(0);
    cv::destroyWindow("Prewitt X");
    cv::destroyWindow("Prewitt Y");
}

void testLaplacian(cv::Mat &gray_img){

    cv::Mat lap, norm_lap;
    laplacianFiltering(gray_img, lap);
    cv::Mat conv;
    cv::normalize(lap, norm_lap, 0, 1, cv::NORM_MINMAX, CV_64FC1);

    norm_lap.convertTo(conv, CV_8U, 255.0);
    cv::imwrite("laplacian_filter.jpg", conv);

    cv::namedWindow("Laplacian", CV_WINDOW_FREERATIO);
    cv::imshow("Laplacian", norm_lap);
    cv::waitKey(0);
    cv::destroyWindow("Laplacian");
}

//Calculates the median of an array of integers
float median(int n, int* x) {
    float temp;
    int i, j;
    // the following two loops sort the array x in ascending order
    for(i=0; i<n-1; i++) {
        for(j=i+1; j<n; j++) {
            if(x[j] < x[i]) {
                // swap elements
                temp = x[i];
                x[i] = x[j];
                x[j] = temp;
            }
        }
    }

    if(n%2==0) {
        // if there is an even number of elements, return mean of the two elements in the middle
        return((x[n/2] + x[n/2 - 1]) / 2.0);
    } else {
        // else return the element in the middle
        return x[n/2];
    }
}

//Makes the median filter for one channel image where k is the size of neighborhood
void medianFilter(cv::Mat &input, cv::Mat &out, unsigned int k){
    int* vec = new int[k*k];
    int orows = input.rows - 2*(k/2);
    int ocols = input.cols - 2*(k/2);
    int count = 0;
    out = cv::Mat::zeros(orows, ocols, CV_8UC1);
    for(int x = 0; x < ocols; x++){
        for(int y = 0; y < orows; y++){
            count = 0;
            for(int i = 0; i < k; i++){
                for(int j = 0; j < k; j++){
                    vec[count] = (int)input.at<uchar>(y+j, x+i);
                    count++;
                }
            }
            out.at<uchar>(y,x) = (int)median(k*k, vec);
        }
    }
}

//Generates a normalized gaussian kernel with neighborhood k and given sigma
cv::Mat gaussianKernelGenerator(int k, double sigma, bool norm){
    cv::Mat ker = cv::Mat::ones(k,k,CV_64FC1);
    double A = 1/(sigma*(2*PI));
    if(k%2 == 0) return ker;
    int center = k/2;
    double xg;
    double yg;
    double g = 0;
    double sum = 0;
    for(int x = 0; x < k; x++){
        for(int y = 0; y < k; y++){
            xg = (double)(x - center);
            yg = (double)(y - center);
            g = A*exp(-(xg*xg + yg*yg)/(2*sigma*sigma));
            sum += g;
            ker.at<double>(y,x) = g;
        }
    }

    if(norm){
        for(int x = 0; x < k; x++){
            for(int y = 0; y < k; y++){
                ker.at<double>(y,x) /= sum;
            }
        }
    }

    return ker;
}

//Convolves one channel image with the given kernel(must be odd)
void convolve2D(cv::Mat &input, cv::Mat &out, cv::Mat ker){
    if(input.type() != CV_8UC1) return;
    cv::Mat kflip;
    cv::flip(ker, kflip, -1);
    int orows = input.rows - 2*(kflip.rows/2);
    int ocols = input.cols - 2*(kflip.cols/2);
    double sum = 0;
    
    out = cv::Mat::zeros(orows, ocols, CV_8UC1);
    for(int x = 0; x < ocols; x++){
        for(int y = 0; y < orows; y++){
            sum = 0;
            for(int i = 0; i < kflip.rows; i++){
                for(int j = 0; j < kflip.cols; j++){
                    sum += ((double)input.at<uchar>(y+i, x+j))*kflip.at<double>(i, j);
                }
            }
            out.at<uchar>(y,x) = (int)sum;
        }
    }
}

//Floating point version convolution
void convolvef2D(cv::Mat &input, cv::Mat &out, cv::Mat ker){
    if(input.type() != CV_8UC1) return;
    cv::Mat kflip;
    cv::flip(ker, kflip, -1);
    int orows = input.rows - 2*(kflip.rows/2);
    int ocols = input.cols - 2*(kflip.cols/2);
    double sum = 0;
    //std::cout << kflip << std::endl;

    out = cv::Mat::zeros(orows, ocols, CV_64FC1);
    for(int x = 0; x < ocols; x++){
        for(int y = 0; y < orows; y++){
            sum = 0;
            for(int i = 0; i < kflip.rows; i++){
                for(int j = 0; j < kflip.cols; j++){
                    sum += ((double)input.at<uchar>(y+i, x+j))*kflip.at<double>(i, j);
                }
            }
            out.at<double>(y,x) = sum;
        }
    }
}

//Convolves an three channel image with the given kernel(odd size)
void convolveColor2D(cv::Mat &input, cv::Mat &out, cv::Mat ker){
    if(input.type() != CV_8UC3) return;
    cv::Mat kflip;
    cv::flip(ker, kflip, -1);
    int orows = input.rows - 2*(ker.rows/2);
    int ocols = input.cols - 2*(ker.cols/2);
    float sum = 0;
    out = cv::Mat::zeros(orows, ocols, CV_8UC3);
    for(int c = 0; c < 3; c++){
        for(int x = 0; x < ocols; x++){
            for(int y = 0; y < orows; y++){
                sum = 0;
                for(int i = 0; i < kflip.rows; i++){
                    for(int j = 0; j < kflip.cols; j++){
                        sum += ((float)(input.at<cv::Vec3b>(y+i, x+j)[c]))*kflip.at<float>(i, j);
                    }
                }
                out.at<cv::Vec3b>(y,x)[c] = (int)sum;
            }
        }
    }
}

void averageFilter(cv::Mat &input, cv::Mat &out, int k){
    cv::Mat ker = cv::Mat::ones(k, k, CV_32FC1);
    float sum = k*k;
    for(int i = 0; i<k; i++){
        for(int j = 0; j<k; j++){
            ker.at<float>(i,j) = ker.at<float>(i,j)/sum; 
        }
    }

    convolve2D(input, out, ker);
}

void weightavgFiltering(cv::Mat &input, cv::Mat &out, int k, float sigma){
    cv::Mat ker = gaussianKernelGenerator(k, sigma);
    convolve2D(input, out, ker);
}

void avgcolorFiltering(cv::Mat &input, cv::Mat &out, int k){
    cv::Mat ker = cv::Mat::ones(k, k, CV_32FC1);
    float sum = k*k;
    for(int i = 0; i<k; i++){
        for(int j = 0; j<k; j++){
            ker.at<float>(i,j) = ker.at<float>(i,j)/sum; 
        }
    }

    cv::Mat color;
    convolveColor2D(input, color, ker);
    cv::Vec3b pix;
    cv::Mat avr = cv::Mat::zeros(color.rows, color.cols, CV_8UC1);
    for(int x = 0; x < color.cols; x++){
        for(int y = 0; y < color.rows; y++){
            pix = color.at<cv::Vec3b>(y,x);
            avr.at<uchar>(y,x) = (pix[0] + pix[1] + pix[2])/3;
        }
    }

    std::vector<cv::Mat> array;
    array.push_back(avr);
    array.push_back(avr);
    array.push_back(avr);
    cv::merge(array, out);
}

void sobelFiltering(cv::Mat &input, cv::Mat &out, int direction){
    //sobel operator
    cv::Mat sob_filt = (cv::Mat_<double>(3,3) << -1, 0, 1, -2, 0, 2, -1, 0, 1); // x direction by default
    
    if(direction != 0){ // y direction
        cv::transpose(sob_filt, sob_filt);
    }
    //std::cout << sob_filt << std::endl;
    convolvef2D(input, out, sob_filt);
}

void prewittFiltering(cv::Mat &input, cv::Mat &out, int direction){
    //prewitt operator
    cv::Mat prew_filt = (cv::Mat_<double>(3,3) << -1, 0, 1, -1, 0, 1, -1, 0, 1); // x direction by default
    
    if(direction != 0){ // y direction
        cv::transpose(prew_filt, prew_filt);
    }
    //std::cout << prew_filt << std::endl;
    convolvef2D(input, out, prew_filt);
}

//Calculetes sobel x, y direction, the magnitude, and the direction
void sobelAll(cv::Mat &input, cv::Mat &gx, cv::Mat &gy, cv::Mat &magn, cv::Mat &direct){
    sobelFiltering(input, gx, 0); //x direction
    sobelFiltering(input, gy, 1); //y direction

    magn = cv::Mat::zeros(gx.rows, gx.cols, CV_64FC1);
    direct = cv::Mat::zeros(gx.rows, gx.cols, CV_64FC1);
    double dx, dy;
    for(int x = 0; x < gx.cols; x++){
        for(int y = 0; y < gx.rows; y++){
            dx = gx.at<double>(y, x);
            dy = gy.at<double>(y, x);
            magn.at<double>(y, x) = sqrt(dx*dx +dy*dy);
            direct.at<double>(y, x) = atan2(dx, dy);
        }
    }
}

//Calculetes prewitt x, y direction, the magnitude, and the direction
void prewittAll(cv::Mat &input, cv::Mat &gx, cv::Mat &gy, cv::Mat &magn, cv::Mat &direct){
    prewittFiltering(input, gx, 0); //x direction
    prewittFiltering(input, gy, 1); //y direction

    magn = cv::Mat::zeros(gx.rows, gx.cols, CV_64FC1);
    direct = cv::Mat::zeros(gx.rows, gx.cols, CV_64FC1);
    double dx, dy;
    for(int x = 0; x < gx.cols; x++){
        for(int y = 0; y < gx.rows; y++){
            dx = gx.at<double>(y, x);
            dy = gy.at<double>(y, x);
            magn.at<double>(y, x) = sqrt(dx*dx + dy*dy);
            direct.at<double>(y, x) = atan2(dx, dy);
        }
    }
}

//generates a laplacian kernel with the aproximation of difference of gaussians
cv::Mat laplacianDoG(int k, float sigma1, float sigma2, bool norm){
    cv::Mat g1 = gaussianKernelGenerator(k, sigma1, false);
    cv::Mat g2 = gaussianKernelGenerator(k, sigma2, false);
    cv::Mat lap = cv::Mat::zeros(k, k, CV_64FC1);
    if(sigma1 > sigma2){
        for(int i = 0; i < g1.rows; i++){
            for(int j = 0; j < g1.cols; j++){
                lap.at<double>(i, j) = g2.at<double>(i,j) - g1.at<double>(i,j);
            }
        }
    }
    else{
        for(int i = 0; i < g1.rows; i++){
            for(int j = 0; j < g1.cols; j++){
                lap.at<double>(i, j) = g1.at<double>(i,j) - g2.at<double>(i,j);
            }
        }
    }

    if(norm){
        double sum = 0;
        for(int x = 0; x < k; x++){
            for(int y = 0; y < k; y++){
                 sum += lap.at<double>(y,x);
            }
        }
        for(int x = 0; x < k; x++){
            for(int y = 0; y < k; y++){
                lap.at<double>(y,x) /= sum;
            }
        }
    }
    
    return lap;
}

void loGFiltering(cv::Mat &input, cv::Mat &output, double sigma1, double sigma2, int k){
    cv::Mat lap_filt = laplacianDoG(k, sigma1, sigma2, false);
    convolvef2D(input, output, lap_filt);
}

void laplacianFiltering(cv::Mat &input, cv::Mat &output){
    cv::Mat lap_filt = (cv::Mat_<double>(3,3) << 0, -1, 0, -1, 4, -1, 0, -1, 0);
    convolvef2D(input, output, lap_filt);
}

//Makes and edge detector, if g_option is equals 0 then apply sobel, otherwise prewitt
//threshold indicates the value for consider an edge, threshold must be betwen 0 and 1.
void edgeDetector(cv::Mat& input, cv::Mat& out, int g_option, double threshold){

    cv::Mat gx, gy, magn, direct;
    if(g_option == 0){
        sobelAll(input, gx, gy, magn, direct);
    }
    else{
        prewittAll(input, gx, gy, magn, direct);
    }

    cv::Mat norm_mag, thresholded;
    cv::normalize(magn, norm_mag, 0, 1, cv::NORM_MINMAX, CV_64F);
    cv::threshold(norm_mag, out, threshold, 1.0, CV_THRESH_BINARY);
}

void drawVectors(cv::Mat &edges, cv::Mat &draw, cv::Mat &dx, cv::Mat &dy, int step){
    std::vector<cv::Mat> array;
    array.push_back(edges);
    array.push_back(edges);
    array.push_back(edges);
    cv::merge(array, draw);
    double alpha = 0.1;
    int ddx, ddy;

    for(int x=step, i = 0; x < edges.cols - step; x += step){
        for(int y=step, j = 0; y < edges.rows - step; y += step){
            if(edges.at<double>(y, x) > 0){
                ddx = x + (int)(alpha*dx.at<double>(y,x));
                ddy = y + (int)(alpha*dy.at<double>(y,x));
                cv::arrowedLine(draw, cv::Point(x,y), cv::Point(ddx, ddy), cv::Scalar(255, 255, 0));   
            }
        }
    }
}